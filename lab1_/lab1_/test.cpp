#include "gtest/gtest.h"
#include "MagicConstants.h"
#include "Trit.h"
#include "TtritSet.h"
namespace TritOne {
	TEST(TestconstructorSet, TrueValue) {
		TritSet set(1000);
		size_t allocLength = set.compacity_trit();
		TritSet set1(2000);
		size_t allocLength1 = set1.compacity_trit();
		ASSERT_GE(allocLength1, allocLength);
		ASSERT_GE(allocLength, 1000 * 2 / 8 / sizeof(uint)); // >=
		ASSERT_GE(allocLength1, 1000 * 2 / 8 / sizeof(uint)); // >=
	}
	TEST(TestSetmemory, TrueValue) {
		TritSet set(1000);
		size_t allocLength = set.compacity_trit();
		set[1000000000] = Trit::Unknown;
		ASSERT_EQ(allocLength, set.compacity_trit()); // =
		if (set[2000000000] == Trit::True) {}
		ASSERT_EQ(allocLength, set.compacity_trit());
		if (set[2000000000] == Trit::False) {}
		ASSERT_EQ(allocLength, set.compacity_trit());
		if (set[2000000000] == Trit::Unknown) {}
		ASSERT_EQ(allocLength, set.compacity_trit());
	}
	TEST(TestCascad, TrueValue) {
		TritSet set(1000);
		size_t allocLength = set.compacity_trit();
		set[99999] = set[100000000] = Trit::True;
		ASSERT_EQ(set[100000000], Trit::True);
		ASSERT_EQ(set[99999], Trit::True);
		ASSERT_LT(allocLength, set.compacity_trit()); // <
		set[10] = Trit::False;
		set[5] = set[10];
		ASSERT_EQ(set[10], Trit::False);
		ASSERT_EQ(set[5], Trit::False);
	}
	TEST(TestInsert, TrueValue) {
		TritSet set(1000);
		ASSERT_EQ(set[20], Trit::Unknown);
		set[20] = Trit::False;
		ASSERT_EQ(set[20], Trit::False);
		set[300] = Trit::True;
		ASSERT_EQ(set[300], Trit::True);
		set[300] = Trit::False;
		ASSERT_EQ(set[300], set[20]);

	}
	TEST(TestCascad2, TrueValue) {
		TritSet set(1000);
		TritSet setA(2000);
		size_t allocLength = set.compacity_trit();
		size_t allocLength1 = setA.compacity_trit();
		ASSERT_GE(allocLength1, allocLength);
		set[20] = Trit::True;
		setA[500] = set[20];
		ASSERT_EQ(set[20], Trit::True);
		ASSERT_EQ(setA[500], Trit::True);
		setA[500] = set[20] = Trit::Unknown;
		ASSERT_EQ(setA[500], set[20]);
	}
	TEST(TestCascad3, TrueValue) {
		TritSet set(1000);
		TritSet setA(1000);
		TritSet setB(1000);
		set[10] = setA[10] = setB[10] = set[50] = Trit::True;
		ASSERT_EQ(setA[10], Trit::True);
		ASSERT_EQ(setB[10], Trit::True);
		ASSERT_EQ(set[10], Trit::True);
		ASSERT_EQ(set[50], Trit::True);
		set[1000] = set[50] = set[60] = Trit::False;
		ASSERT_EQ(set[1000], set[50], set[60]);
		set[1000] = set[55] = set[64] = setB[64] = Trit::True;
		ASSERT_EQ(setB[64], Trit::True);
		ASSERT_EQ(set[64], Trit::True);
		ASSERT_EQ(set[55], Trit::True);
		ASSERT_EQ(set[1000], Trit::True);
	}
	TEST(Trit, TrueValue) {
		TritSet set(1000);
		set[100] = Trit::True;
		ASSERT_EQ(set[100], Trit::True);
		Trit b = set[100];
		ASSERT_EQ(b, Trit::True);
		set[100] = set[50] = set[999] = Trit::False;
		ASSERT_EQ(set[50], Trit::False);
		Trit a = set[50];
		ASSERT_EQ(a, Trit::False);

	}
	TEST(Test�onjunction, TrueValue) {
		TritSet setA(1000);
		TritSet setB(2000);
		TritSet setC = setA & setB;
		ASSERT_EQ(setB.compacity_trit(), setC.compacity_trit());
		ASSERT_GE(setC.compacity_trit(), setA.compacity_trit());
	}
	TEST(TestDisjunction, TrueValue) {
		TritSet setA(1000);
		TritSet setB(2000);
		TritSet setC = setA | setB;
		ASSERT_EQ(setB.compacity_trit(), setC.compacity_trit());
		ASSERT_GE(setC.compacity_trit(), setA.compacity_trit());
	}
	TEST(TestDenial, TrueValue) {
		TritSet setA(1000);
		TritSet setC = ~setA;
		ASSERT_EQ(setA.compacity_trit(), setC.compacity_trit());
		TritSet setB(2000);
		setB[1000] = Trit::True;
		ASSERT_EQ(setB[1000], Trit::True);
		setB = ~setB;
		ASSERT_EQ(setB[1000], Trit::False);
	}
	TEST(TrueCount, TrueValue) {
		TritSet set(1000);
		auto card = set.cardinality();
		ASSERT_EQ(card[Trit::True], 0);
		set[1000] = Trit::True;
		card = set.cardinality();
		ASSERT_EQ(card[Trit::True], 1);
		set[200] = Trit::True;
		card = set.cardinality();
		ASSERT_EQ(card[Trit::True], 2);
	}
	TEST(FalseCount, TrueValue) {
		TritSet set(1000);
		auto card = set.cardinality();
		ASSERT_EQ(card[Trit::False], 0);
		set[1000] = Trit::False;
		card = set.cardinality();
		ASSERT_EQ(card[Trit::False], 1);
		set[200] = Trit::False;
		card = set.cardinality();
		ASSERT_EQ(card[Trit::False], 2);
	}
	TEST(UnknowCount, TrueValue) {
		TritSet set(1000);
		set[100000000] = Trit::Unknown;
		set[1000000] = Trit::False;
		auto card = set.cardinality();
		ASSERT_EQ(card[Trit::Unknown], 1000000 + 1 - 1);
		TritSet setA(1000);
		setA[1000] = Trit::True;
		auto card2 = setA.cardinality();
		ASSERT_EQ(card2[Trit::Unknown], 1000 + 1 - 1);
	}
	TEST(TestShrink, TrueValue) {
		TritSet set(1000);
		set[50] = Trit::True;
		ASSERT_EQ(set[50], Trit::True);
		ASSERT_EQ(set.lenght(), 50 + 1);
		set[500] = Trit::False;
		ASSERT_EQ(set[500], Trit::False);
		ASSERT_EQ(set.lenght(), 500 + 1);
		set[900] = set[500];
		ASSERT_EQ(set[900], Trit::False);
	}
	TEST(TestAll, TrueValue) {
		TritSet set(1000);
		size_t allocLength = set.compacity_trit();
		ASSERT_GE(allocLength, 1000 * 2 / 8 / sizeof(uint)); // >=
		set[1000000000] = Trit::Unknown;
		ASSERT_EQ(allocLength, set.compacity_trit()); // =
		if (set[2000000000] == Trit::True) {}
		ASSERT_EQ(allocLength, set.compacity_trit());
		set[99999] = set[100000000] = Trit::True;
		ASSERT_EQ(set[100000000], Trit::True);
		ASSERT_EQ(set[99999], Trit::True);
		ASSERT_LT(allocLength, set.compacity_trit()); // <
		allocLength = set.compacity_trit();
		set[100000000] = Trit::Unknown;
		set[1000000] = Trit::False;
		ASSERT_EQ(set[1000000], Trit::False);
		ASSERT_EQ(allocLength, set.compacity_trit());
		set.shrink();
		ASSERT_GT(allocLength, set.compacity_trit()); // >
		TritSet setA(1000);
		TritSet setB(2000);
		TritSet setC = setA & setB;
		ASSERT_EQ(setB.compacity_trit(), setC.compacity_trit());
		auto card = set.cardinality();
		ASSERT_EQ(card[Trit::True], 1);
		ASSERT_EQ(card[Trit::False], 1);
		ASSERT_EQ(card[Trit::Unknown], 1000000 + 1 - 2);
		set[10] = Trit::False;
		ASSERT_EQ(set[10], Trit::False);
		set.trim(1000);
		ASSERT_EQ(set[10], Trit::False);
		ASSERT_EQ(set[99999], Trit::Unknown);
		ASSERT_EQ(set.lenght(), 10 + 1);
	}
}