#pragma once
#include "Block.h"
#include "Exception.h"
#include "BlockException.h"
#include "Constants.h"
namespace Blocks {
	class Dump : public Blocks {
	public:
		std::list<std::wstring>& execute(std::list <std::wstring>& text, std::vector<std::wstring>& args) override;
		BlockType gettype() override;
		~Dump();
	private:
		std::wofstream out;
	};
}
