#pragma once
#include <list>
#include <vector>
#include <iostream>
#include "BlocType.h"
#include "fstream"
#include <string>
#include <fstream>
namespace Blocks {
	class Blocks {
	public:
		virtual std::list<std::wstring>& execute(std::list <std::wstring>& text, std::vector<std::wstring>& args) = 0;
		virtual BlockType gettype() = 0;
	};
}