#pragma once
#include <iostream>
#include <list>
#include <vector>
#include <fstream>
#include "Exception.h"
#include "BlockException.h"
namespace WorkFlow {
	class WorkFlowParser {
	private:
		std::wifstream input;
		std::wstring str;
		std::list<std::pair<std::wstring, std::vector<std::wstring>>> manual;
		std::vector<std::wstring> command_sequence;
	public:
		void execute();
		WorkFlowParser(std::string file_name);
		std::list <std::pair<std::wstring, std::vector<std::wstring>>> get_list();
		std::vector<std::wstring> get_command();
		~WorkFlowParser();
	};
}