#include "Write.h"
#include "Constants.h"
#include <codecvt>
namespace Blocks {
	std::list<std::wstring>& WriteFile::execute(std::list<std::wstring>& text, std::vector<std::wstring>& args)
	{
		const std::locale utf8_locale = std::locale(std::locale(), new std::codecvt_utf8<wchar_t>());
		out1.open(args[Constants::START_FILE]);
		out1.imbue(utf8_locale);
		if (!out1.is_open()) 
		{
			throw Exception::BlocksException (Constants::ERROR_FILE);
		}
		if (args.size() > Constants::MORE_THAN_TWO_ARGUMENTS) {
			throw Exception::BlocksException (Constants::ERROR_MANY_ARGUMENTS);
		}
		for (auto it = text.begin(); it != text.end(); ++it) {
			out1 << (*it) << std::endl;
		}
		return text;
	}
	BlockType WriteFile::gettype()
	{
		return BlockType::OUT;
	}
	WriteFile::~WriteFile()
	{
		out1.close();
	}
}