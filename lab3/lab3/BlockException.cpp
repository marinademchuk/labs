#include "BlockException.h"
#include <iostream>
namespace Exception {
	BlocksException::BlocksException(const char* _text)
	{
		text = _text;
	}
	const char* BlocksException::what() {
		return text;
	}
	
}