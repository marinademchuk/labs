#include "Read.h"
#include <codecvt>
#include "Exception.h"
#include "BlockException.h"
#include "Constants.h"
namespace Blocks{
std::list<std::wstring>& ReadFile::execute(std::list<std::wstring>&text, std::vector<std::wstring>&args)
{
	const std::locale utf8_locale = std::locale(std::locale(), new std::codecvt_utf8<wchar_t>());
		input.open(args[Constants::START_FILE]);
		input.imbue(utf8_locale);

	if (!input.is_open()) {
		throw Exception::BlocksException (Constants::ERROR_FILE);
	}
	if (args.size() > Constants::MORE_THAN_TWO_ARGUMENTS) {
		throw Exception::BlocksException (Constants::ERROR_MANY_ARGUMENTS);
	}
	while (!input.eof()) {
		std::wstring str;
		getline(input, str);
		text.push_back(str);
	}

	return text;
}
BlockType ReadFile::gettype()
{

	return BlockType::IN;
}
ReadFile::~ReadFile()
{
	input.close();
}
}