#pragma once
#include <string>
#include <fstream>
#include <ctype.h>
#include "Block.h"
namespace Blocks {
	class ReadFile : public Blocks {
	public:
		std::list<std::wstring>& execute(std::list <std::wstring>& text, std::vector<std::wstring>& args) override;
		BlockType gettype() override;
		~ReadFile();
	private:
		std::wifstream input;
	};
}
