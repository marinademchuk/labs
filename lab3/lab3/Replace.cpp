#include "Replace.h"
#include "Constants.h"
namespace Blocks {
	std::list<std::wstring>& Replace::execute(std::list<std::wstring>& text, std::vector<std::wstring>& args)
	{
		std::wstring word1 = args[Constants::START_FILE];
		std::wstring word2 = args[Constants::TWO_ARGUMENT];
		if (args.size() > Constants::MORE_THAN_THREE_ARGUMENTS) {
			throw Exception::BlocksException(Constants::ERROR_MANY_ARGUMENTS);
		}
		for (auto it = text.begin(); it != text.end(); ++it) {
			std::wstring word = L"";
			for (int i = 0; i < (*it).size(); ++i) {
				if (((*it)[i] >= Constants::START_LITTLE_ENGLISH && (*it)[i] <= Constants::END_LITTLE_ENGLISH) || (
					((*it)[i] >= Constants::START_BIG_ENGLISH && (*it)[i] <= Constants::END_BIG_ENGLISH)) ||
					((*it)[i] >= Constants::START_BIG_RUSSIAN && (*it)[i] <= Constants::END_BIG_RUSSIAN) ||
					((*it)[i] >= Constants::START_LITTLE_RUSSIAN && (*it)[i] <= Constants::END_LITTLE_RUSSIAN) ||
					((*it)[i] >= Constants::START_NUMBER && (*it)[i] <= Constants::END_NUMBER))
				{
					word += (*it)[i];
					if (word == word1) {
						auto pos = (*it).find(word1);
						(*it).erase(pos, word1.size());
						(*it).insert(pos, word2);
						word = L"";
					}
				}
				else {
					word = L"";
				}
			}

		}
		return text;
	}
	BlockType Replace::gettype()
	{
		return BlockType::INOUT;
	}
}