#pragma once
#include <string>
namespace Constants {
	const int MAX_ARGC = 2;
	const int FILE_FAILED = 2;
	const int END_PROFGRAM = 0;
	const wchar_t START_NUMBER = L'0';
	const wchar_t END_NUMBER = L'9';
	const wchar_t START_LITTLE_ENGLISH = L'a';
	const wchar_t END_LITTLE_ENGLISH = L'z';
	const wchar_t START_BIG_ENGLISH = L'A';
	const wchar_t END_BIG_ENGLISH = L'Z';
	const wchar_t START_LITTLE_RUSSIAN = L'�';
	const wchar_t END_LITTLE_RUSSIAN = L'�';
	const wchar_t START_BIG_RUSSIAN = L'�';
	const wchar_t END_BIG_RUSSIAN = L'�';
	const wchar_t DOT_IN_WORD = L'.';
	const wchar_t UNDERLINE_IN_WORD = L'_';
	const int NULL_WORD = 0;
	const int START_FILE = 1;
	const int FLAG = 1;
	const int END_FILE = 2;
	const int BLOCK_NAME = 0;
	const int ALIGNMENT = 1;
	const int TWO_ARGUMENT = 2;
	const int ERROR_PROGRAMM = 1;
	const std::wstring READ = L"readfile";
	const std::wstring WRITE = L"writefile";
	const std::wstring  GREP = L"grep";
	const std::wstring SORT = L"sort";
	const std::wstring REPLACE = L"replace";
	const std::wstring DUMP = L"dump";
	const char* ERROR_FILE = "sorry, file is not open!";
	const char* ERROR_MANY_ARGUMENTS = "many arguments";
	const char* ERROR_WRONG_SEQUENCE = "wrong sequence";
	const int MORE_THAN_TWO_ARGUMENTS = 2;
	const int MORE_THAN_ONE_ARGUMENTS = 1;
	const int MORE_THAN_THREE_ARGUMENTS = 3;

}