#pragma once
#include "Block.h"
#include "Exception.h"
#include "BlockException.h"
namespace Blocks {
	class WriteFile : public Blocks {
	public:
		std::list<std::wstring>& execute(std::list <std::wstring>& text, std::vector<std::wstring>& args) override;
		BlockType gettype() override;
		~WriteFile();
	private:
		std::wofstream out1;
	};
}
