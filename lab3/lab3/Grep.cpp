#include "Grep.h"
#include "Constants.h"
namespace Blocks {
	std::list<std::wstring>& Grep::execute(std::list<std::wstring>& text, std::vector<std::wstring>& args)
	{
		std::list <std::wstring> my_list;
		std::wstring word = args[Constants::START_FILE];
		if (args.size() > Constants::MORE_THAN_TWO_ARGUMENTS) {
			throw Exception::BlocksException (Constants::ERROR_MANY_ARGUMENTS);
		}
		for (auto it = text.begin(); it != text.end(); ++it) {
			if ((*it).find(word) != std::string::npos) {
				my_list.push_back((*it));
			}
		}
		return my_list;
	}
	BlockType Grep::gettype()
	{
		return BlockType::INOUT;
	}
}