#include "ParserWorkFlow.h"
#include <fstream>
#include <iostream>
#include <string>
#include "Constants.h"
#include <codecvt>
namespace WorkFlow {

	WorkFlowParser::WorkFlowParser(std::string file_name) {
		const std::locale utf8_locale = std::locale(std::locale(), new std::codecvt_utf8<wchar_t>());
		input.open(file_name);
		input.imbue(utf8_locale);
		if (!input.is_open()) {
			throw Exception::BlocksException (Constants::ERROR_FILE);
		}
	}
	void WorkFlowParser::execute()
	{
		int f = Constants::NULL_WORD;
		while (!input.eof())
		{
			getline(input, str);
			if (str == L"desc") { continue; }
			else
				if (str == L"csed")
				{
					f = Constants::FLAG;
					continue;
				}
			if (f == Constants::FLAG) {
				int j = Constants::NULL_WORD;
				std::wstring word = L"";
				for (int i = 0; i <= str.length(); ++i) {
					if (str[i] >= Constants::START_NUMBER && str[i] <= Constants::END_NUMBER) {
						word += str[i];
						j = Constants::FLAG;
					}
					if (str[i] == L' ' && j == Constants::FLAG) {
						command_sequence.push_back(word);
						word = L"";
						j = Constants::NULL_WORD;
					}
				}
				command_sequence.push_back(word);
			}
			else {
				if (f != Constants::FLAG) {
					std::wstring word = L"";
					std::wstring word1 = L"";
					int j = Constants::NULL_WORD;
					std::vector<std::wstring> my_vector;
					int flag = Constants::NULL_WORD;
					for (int i = 0; i <= str.length(); ++i) {
						if (str[i] == L'=') {
							flag = Constants::FLAG;
							continue;
						}
						if (flag == Constants::NULL_WORD) {
							if (str[i] >= Constants::START_NUMBER && str[i] <= Constants::END_NUMBER) {

								word += str[i];
							}
						}
						if (flag == Constants::FLAG) {
							if (str[i] != L' ')
							{
								if ((str[i] >= Constants::START_LITTLE_ENGLISH && str[i] <= Constants::END_LITTLE_ENGLISH) ||
									(str[i] == Constants::DOT_IN_WORD) || (str[i] == Constants::UNDERLINE_IN_WORD) ||
									(str[i] >= Constants::START_BIG_ENGLISH && str[i] <= Constants::END_BIG_ENGLISH) ||
									(str[i] >= Constants::START_LITTLE_RUSSIAN && str[i] <= Constants::END_LITTLE_RUSSIAN) ||
									(str[i] >= Constants::START_BIG_RUSSIAN && str[i] <= Constants::END_BIG_RUSSIAN) ||
									(str[i] >= Constants::START_NUMBER && str[i] <= Constants::END_NUMBER))
									word1 += str[i];
								j = Constants::FLAG;
							}
							else {
								if (j == Constants::FLAG) {
									my_vector.push_back(word1);
									word1 = L"";
									j = Constants::NULL_WORD;
								}
							}

						}
					}
					my_vector.push_back(word1);
					manual.push_back(make_pair(word, my_vector));
				}
			}
		}
	}
	std::list <std::pair<std::wstring, std::vector<std::wstring>>>  WorkFlowParser::get_list()
	{
		return manual;
	}
	std::vector<std::wstring> WorkFlowParser::get_command()
	{
		return command_sequence;
	}
	WorkFlowParser::~WorkFlowParser()
	{
		input.close();
	}
}