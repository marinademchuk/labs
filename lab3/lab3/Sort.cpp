#include "Sort.h"
#include "Constants.h"
namespace Blocks {
		bool Comparator(std::wstring a, std::wstring b)
		{
			return a < b ;
		}
	std::list<std::wstring>& Sort::execute(std::list<std::wstring>& text, std::vector<std::wstring>& args)
	{ 
		if (args.size() > Constants::MORE_THAN_ONE_ARGUMENTS) {
			throw Exception::BlocksException (Constants::ERROR_MANY_ARGUMENTS);
		}
		text.sort(Comparator);
		return text;
	}
	BlockType Sort::gettype()
	{
		return BlockType::INOUT;
	}
}