#pragma once
#include <iostream>
namespace Exception {
	class MyException {
	protected:
		const char* text;
	public:
		virtual const char* what() = 0;
	};
}