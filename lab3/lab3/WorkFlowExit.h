#pragma once
#include <iostream>
#include <list>
#include <vector>
#include "Factory.h"
#include "DumpCreature.h"
#include "GrepCreature.h"
#include "ReaderCreature.h"
#include "ReplaceCreature.h"
#include "SortCreature.h"
#include "WriteCreature.h"
#include "BlocType.h"
#include <map>
namespace Exit {
	class WorkFlowExit {
	public:
		WorkFlowExit(std::vector<std::wstring>&, 
			std::list <std::pair<std::wstring, std::vector<std::wstring>>>&);
		void DoWorkFlow(std::map<std::wstring, Blocks::Factory*> map_blocks);
	private:
		std::vector<std::wstring> my_vector;
		std::list <std::pair<std::wstring, std::vector<std::wstring>>> text;
		std::list<std::wstring> my_list;
	};
}
