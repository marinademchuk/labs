#include "WorkFlowExit.h"
#include "Constants.h"
#include "Exception.h"
#include "BlockException.h"
namespace Exit {
	WorkFlowExit::WorkFlowExit(std::vector<std::wstring>& vector, std::list<std::pair<std::wstring, std::vector<std::wstring>>>& list1)
		: my_vector{ vector },
		text{ list1 }
	{
	}
	void WorkFlowExit::DoWorkFlow(std::map<std::wstring, Blocks::Factory*> map_blocks) 
	{
		std::list <std::pair<Blocks::Blocks*, std::vector<std::wstring>>> block_object;
		for (auto it : my_vector) 
		{
			for (auto i : text) {
				if (it == i.first) {
					block_object.emplace_back(map_blocks[i.second[0]]->createBlock(), i.second);
				}
			}
		}
		int j = 0;
		for (auto it = block_object.begin(); it != block_object.end(); ++it) {
				if ((it->first->gettype() == Blocks::BlockType::IN && it != block_object.begin()) ||
					(it->first->gettype() == Blocks::BlockType::OUT && j != block_object.size() - Constants::ALIGNMENT) ||
					(it->first->gettype() == Blocks::BlockType::INOUT && (it == block_object.begin() || j == block_object.size() - Constants::ALIGNMENT)))
				{
					for (auto i = block_object.begin(); i != block_object.end(); ++i) {
						delete i->first;
					}
					throw Exception::BlocksException (Constants::ERROR_WRONG_SEQUENCE);
				}
				++j;
		}
		std::list <std::wstring> my_text;
		for (auto& it : block_object) 
		{
			it.first->execute(my_text, it.second);
		}


		
	}
}