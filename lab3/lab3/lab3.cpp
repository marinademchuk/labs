﻿#include <iostream>
#include <fstream>
#include "ParserWorkFlow.h"
#include <list>
#include <vector>
#include "WorkFlowExit.h"
#include "Constants.h"
#include "BlockException.h"
#include "Exception.h"
#include <map>
#include "DumpCreature.h"
#include "GrepCreature.h"
#include "ReaderCreature.h"
#include "ReplaceCreature.h"
#include "SortCreature.h"
#include "WriteCreature.h"
#include "Factory.h"
int main(int argc, char* argv[])
{
	if (argc != Constants::MAX_ARGC) {
		std::cout << "eror" << std::endl;
		return Constants::END_PROFGRAM;
	} 
	std::map<std::wstring, Blocks::Factory*> my_map;
	try {
		WorkFlow::WorkFlowParser file(argv[Constants::START_FILE]);
		file.execute();
		std::list <std::pair<std::wstring, std::vector<std::wstring>>> text = file.get_list();
		std::vector <std::wstring> my_vector = file.get_command();
		Exit::WorkFlowExit my_exit(my_vector, text);
		//std::map<std::string, Blocks::Factory*> my_map;
		my_map[Constants::READ] = new Blocks::ReaderCreature();
		my_map[Constants::GREP] = new Blocks::GrepCreature();
		my_map[Constants::DUMP] = new Blocks::DumpCreature();
		my_map[Constants::REPLACE] = new Blocks::ReplaceCreature();
		my_map[Constants::SORT] = new Blocks::SortCreature();
		my_map[Constants::WRITE] = new Blocks::WriteCreature();
		my_exit.DoWorkFlow(my_map);
	}
	catch (Exception::BlocksException& my_error) {
		std::cout << my_error.what() << std::endl;
		for (auto it = my_map.begin(); it != my_map.end(); ++it) {
			delete it->second;
		}
		return Constants::ERROR_PROGRAMM;
	}
	for (auto it = my_map.begin(); it != my_map.end(); ++it) {
		delete it->second;
	}
	return Constants::END_PROFGRAM;
}