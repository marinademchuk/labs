#pragma once
#include "Exception.h"
#include <iostream>
#include <string>
namespace Exception {
	class BlocksException : public MyException {
	public:
		BlocksException(const char* _text);
		const char* what() override;
	};
}