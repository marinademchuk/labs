#include "Dump.h"
#include <codecvt>
namespace Blocks {
	std::list<std::wstring>& Dump::execute(std::list<std::wstring>& text, std::vector<std::wstring>& args)
	{
		const std::locale utf8_locale = std::locale(std::locale(), new std::codecvt_utf8<wchar_t>());
		out.open(args[Constants::START_FILE]);
		out.imbue(utf8_locale);
		if (!out.is_open()) {
			
			throw Exception::BlocksException (Constants::ERROR_FILE);
		}
		if (args.size() > Constants::MORE_THAN_TWO_ARGUMENTS) {
			throw Exception::BlocksException (Constants::ERROR_MANY_ARGUMENTS);
		}
		for (auto it = text.begin(); it != text.end(); ++it) {
			out << (*it) << std::endl;
		}
		return text;
}
	BlockType Dump::gettype()
	{
		return BlockType::INOUT;
	}
	Dump::~Dump()
	{
		out.close();
	}
}